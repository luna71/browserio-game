class Entity {
    constructor(ctx){
        this.ctx = ctx;
        this.Size = {
            width: 0,
            height: 0,
        };

        // Position of an object is at the center of the object, not the top left
        this.Position = {
            x: 0,
            y: 0,
            rotation: 0,
        };

        this.Properties = {
            collidable: false,
        }
    }

    Draw(){
        console.log("Draw");
    }

}

module.exports = Entity;