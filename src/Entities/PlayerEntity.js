let Entity = require('./Entity.js');

class PlayerEntity extends Entity {
    constructor(ctx){
        super(ctx);
        this.Size.height = 4;
        this.EntityType = "PlayerEntity";
    }

    Draw(){
        this.ctx.beginPath();
        this.ctx.arc(this.Position.x, this.Position.y, this.Size.height, 0, 2 * Math.PI);
        this.ctx.stroke();
    }

}

module.exports = PlayerEntity;