const express = require('express');
const app = express();
const path = require("path");
const baseRouter = require('./routes/baseRouter.js');
const http = require('http').createServer(app);
const io = require('socket.io')(http);

const PlayerEntity = require('./Entities/PlayerEntity.js');

const port = process.env.PORT || 80;

let Entities = [];


app.engine('jade', require('jade').__express);
app.set('view engine','pug');
app.set('views', path.join(__dirname, './../views'));

app.use('/', baseRouter);

io.on('connection', (socket)=>{

    let player = new PlayerEntity();
    let controls = {};
    Entities.push(player);

    socket.on('control', (msg)=>{
        controls = msg;
    });

    socket.on('disconnect', ()=> {
        Entities.splice(Entities.indexOf(player), 1);
    });

    setInterval(()=>{
        socket.emit('entity', Entities)
        if (controls.UP)
            player.Position.y -= 1;
        else if(controls.DOWN)
            player.Position.y += 1;
        if (controls.LEFT)
            player.Position.x -= 1;
        else if(controls.RIGHT)
            player.Position.x += 1;
    }, 1/60);
});

app.use(express.static(__dirname + './../public'));
http.listen(port, ()=> console.log(`Server started on port ${port}`));