let inputStates = {
    UP: false,
    DOWN: false,
    LEFT: false,
    RIGHT: false,
    LEFT_CLICK: false,
    RIGHT_CLICK: false,
    SPACE: false,
}

const inputKeys = {
    ARROW_DOWN: 40,
    ARROW_RIGHT: 39,
    ARROW_UP: 38,
    ARROW_LEFT: 37,
    W: 87,
    A: 65,
    S: 83,
    D: 68,
    SPACE: 32,
}

export default function getInputStates() {
    return inputStates;
}

document.addEventListener("keydown", (input)=> {
    switch(input.keyCode) {
        case inputKeys.ARROW_LEFT:
        case inputKeys.A:
            inputStates.LEFT = true;
            break;
        case inputKeys.ARROW_RIGHT:
        case inputKeys.D:
            inputStates.RIGHT = true;
            break;
        case inputKeys.ARROW_UP:
        case inputKeys.W:
            inputStates.UP = true;
            break;
        case inputKeys.ARROW_DOWN:
        case inputKeys.S:
            inputStates.DOWN = true;
            break;
        case inputKeys.SPACE:
            inputStates.SPACE = true;
            break;
    }
});

document.addEventListener("keyup", (input)=> {
    switch(input.keyCode) {
        case inputKeys.ARROW_LEFT:
        case inputKeys.A:
            inputStates.LEFT = false;
            break;
        case inputKeys.ARROW_RIGHT:
        case inputKeys.D:
            inputStates.RIGHT = false;
            break;
        case inputKeys.ARROW_UP:
        case inputKeys.W:
            inputStates.UP = false;
            break;
        case inputKeys.ARROW_DOWN:
        case inputKeys.S:
            inputStates.DOWN = false;
            break;
        case inputKeys.SPACE:
            inputStates.SPACE = false;
            break;
    }
});

document.addEventListener("mousedown", (input) => {
    switch(input.which) {
        case 1:
            inputStates.LEFT_CLICK = true;
            break;
        case 3:
            inputStates.RIGHT_CLICK = true;
            break;
    }
    input.preventDefault();
    input.stopPropagation();
});

document.addEventListener("mouseup", (input) => {
    switch(input.which) {
        case 1:
            inputStates.LEFT_CLICK = false;
            break;
        case 3:
            inputStates.RIGHT_CLICK = false;
            break;
    }
    input.preventDefault();
    input.stopPropagation();
});

document.getElementById('gameFrame').addEventListener('contextmenu', event => event.preventDefault());

