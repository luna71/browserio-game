import Entity from "./Entity.js";

export default class PlayerEntity extends Entity {
    constructor(ctx){
        super(ctx);
        this.Size.height = 4;
    }

    Draw(){
        this.ctx.beginPath();
        this.ctx.arc(this.Position.x, this.Position.y, this.Size.height, 0, 2 * Math.PI);
        this.ctx.stroke();
    }

}