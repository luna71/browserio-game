import getInputStates from '/javascript/controls.js'
import Entity from '/javascript/Entities/Entity.js';
import PlayerEntity from '/javascript/Entities/PlayerEntity.js';

var socket = io();

let inputStates = getInputStates();
let ctx = null;
let canvas = null;

let playerEntity = null;
let entities = []


function gameLoop() {
    socket.emit('control', inputStates);

    if (ctx == null || canvas == null)
        return;
    
    ctx.clearRect(0, 0, canvas.width, canvas.height)
    entities.forEach((Entity)=>Entity.Draw())
}

document.addEventListener("DOMContentLoaded", function() {
    canvas = document.getElementById('gameFrame');
    canvas.width = window.innerWidth;
    canvas.height = window.innerHeight;
    ctx = canvas.getContext('2d');

    playerEntity = new PlayerEntity(ctx);
    entities.push(playerEntity);

    setInterval(gameLoop, 1000/60);
});

socket.on('entity', (msg)=> {
    entities = [];
    msg.forEach((entity)=>{
        switch(entity.EntityType) {
            case "PlayerEntity":
                let newEntity = new PlayerEntity(ctx);
                newEntity.Size = entity.Size;
                newEntity.Position = entity.Position;
                newEntity.Properties = entity.Properties;
                entities.push(newEntity)
        }
    });
})
